const button = document.createElement('button');
button.textContent = 'získej polohu';

document.body.appendChild(button);

const success = (data) => console.log(data);
const error = (err) => console.error(err);

const getLocation = () => {
  const geo = navigator.geolocation;

  const options = {
    enableHighAccuracy: true,
    timeout: 3000,
  };

  geo.getCurrentPosition(success, error, options);
};

button.addEventListener('click', getLocation);
