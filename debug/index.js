/* eslint-disable vars-on-top */
/* eslint-disable func-names */
/* eslint-disable prefer-arrow-callback */
/* eslint-disable no-var */
var gpx = `<gpx xmlns="http://www.topografix.com/GPX/1/1" version="1.1">
  <wpt lon="14.0579684079" lat="49.7448585927">
    <name>Pičín</name>
  </wpt>
  <wpt lon="14.3711069226" lat="50.2368152887">
    <name>Kozomín</name>
  </wpt>
  <wpt lon="14.2992331088" lat="49.8945144564">
    <name>Řitka</name>
  </wpt>
  <wpt lon="14.3768186867" lat="50.104515329">
    <name>Dejvice</name>
  </wpt>
  <wpt lon="15.7047213614" lat="50.0601475686">
    <name>Rybitví</name>
  </wpt>
</gpx>`;

var points = [];

/**
 * Parsuje XML prvky (wpt) na body
 * @param {NodeList} nodes Prvky wpt (waypoint), které budu parsovat;
 * @returns {Array} pole bodů s názvy
 */
function addPoints(nodes) {
  var myPoints = Array
    .from(nodes)
    .map(function (node) {
      if (node.nodeType !== Node.ELEMENT_NODE) {
        return undefined;
      }

      return {
        name: node.getElementsByTagName('name')[0]
          ? node.getElementsByTagName('name')[0].textContent
          : '',
        lat: node.attributes.lat.nodeValue,
        lon: node.attributes.lon.nodeValue,
      };
    })
    .filter(function (item) {
      return !!item;
    });

  return myPoints;
}

window.addEventListener('load', function () {
  // pojďme odkrokovat a zjistit, kde vzniká chyba
  // debugger;
  var domParser = new DOMParser();
  var dom = domParser.parseFromString(gpx, 'application/xml');
  var root = dom.firstChild;
  var nodes = root.childNodes;
  points = addPoints(nodes);

  // Co mám v points? Mám toho správný počet? Zkontrolujeme to po vytvoření!
  console.log('points', points);

  var center = [49, 16];
  var zoom = 15;

  var map = L
    .map('mapa')
    .setView(center, zoom);

  L
    .tileLayer('https://tile.openstreetmap.org/{z}/{x}/{y}.png', {
      maxZoom: 19,
      attribution: '&copy; <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a>',
    })
    .addTo(map);

  var markers = points.map(function (point) {
    var marker = L
      .marker([point.lat, point.lon])
      .addTo(map);

    var element = marker.getElement();
    element.title = point.name;

    return marker;
  });

  console.log('markers:', markers);

  var group = L.featureGroup(markers);
  var bounds = group.getBounds();

  console.log('bounds:', bounds);

  map.fitBounds(bounds, { padding: [10, 10] });
});
