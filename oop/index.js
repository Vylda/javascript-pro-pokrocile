/* eslint-disable no-new */
/* eslint-disable prefer-arrow-callback */
/* eslint-disable no-unused-vars */
/* eslint-disable no-underscore-dangle */
/* eslint-disable no-extend-native */
/* eslint-disable object-shorthand */
/* eslint-disable func-names */
/* eslint-disable no-new-object */
/* eslint-disable vars-on-top */
/* eslint-disable no-new-wrappers */
/* eslint-disable no-var */

var a = 1.5225;
var b = new Number(1.5225);

// eslint-disable-next-line eqeqeq
console.log(a == b);
console.log(a === b);

console.log(a.toFixed(3));
console.log(b.toFixed(3));

var o1 = new Object();
var o2 = {};
console.log(o1, o2);

var man1 = {
  age: 33,
  getSalutation: function () {
    return 'Ahoj, jsem člověk 1. generace.';
  },
};

console.log(man1, man1.age, man1.getSalutation());

var Man = function (age, name) {
  this.age = age;
  this.name = name;

  this.salutate = function () {
    return `Ahoj, jmenuji se ${this.name} a je mi ${this.age} let.`;
  };
};

var karel = new Man(78, 'Karel Gott');

console.log(karel.salutate(), karel.age);
karel.name = 'Karel Hála';
karel.age = 65;
karel.jeVul = false;

console.log(karel.salutate(), karel.jeVul);

var ProtoMan = function (age, name) {
  this.age = age;
  this.name = name;
};

ProtoMan.prototype = {
  salutate: function () {
    return `Ahoj, jmenuji se ${this.name} a je mi ${this.age} let.`;
  },
};

var standa = new ProtoMan(68, 'Standa Hložek');

console.log(standa, standa.age, standa.salutate());

var teta = new ProtoMan(50, 'Běta');

console.log(teta.salutate());

ProtoMan.prototype.sayBye = function () {
  return `Jen krátká návštěva potěší, říkám jako ${this.name}.`;
};

console.log(standa.sayBye());
console.log(teta.sayBye());

Number.prototype.isEven = function () {
  return !(this % 2);
};

console.log((5).isEven(), (4).isEven());

ProtoMan.drunk = function () {
  console.log('Glgám škopek.');
};

ProtoMan.drunk();

var ProtoManEncapsulated = function (age, name) {
  this._age = age;
  this._name = name;
};

ProtoManEncapsulated.getClassName = function () {
  return 'ProtoManEncapsulated';
};

ProtoManEncapsulated.prototype = {
  salutate: function () {
    return `Ahoj, jmenuji se ${this._name} a je mi ${this._age} let.`;
  },
  getAgeName: function () {
    return {
      age: this._age,
      name: this._name,
    };
  },
  toString: function () {
    return `${this._age}/${this._name}/${ProtoManEncapsulated.getClassName()}/${ProtoManEncapsulated.name}`;
  },
};

var petr = new ProtoManEncapsulated(64, 'Petr Kotvald');

console.log(petr.getAgeName());
console.log(`Objekt ${petr}`);

var Employee = function (age, name, salary) {
  ProtoManEncapsulated.call(this, age, name);

  this._salary = salary;
};

Employee.prototype = Object.create(ProtoManEncapsulated.prototype);

Object.assign(
  Employee.prototype,
  {
    getSalary: function () {
      return this._salary;
    },
    setSalary: function (newSalary) {
      this._salary = newSalary;
    },
  },
);

var pepaZDepa = new Employee(36, 'Josef von Banhoff', 25000);

console.log(pepaZDepa);
console.log(pepaZDepa.getAgeName());
console.log(pepaZDepa.getSalary());
pepaZDepa.setSalary(30000);
console.log(pepaZDepa.getSalary());

var SmartEmployee = function (age, name, salary) {
  this._parent = Employee;
  this._parent.call(this, age, name, salary);
};

SmartEmployee.prototype = Object.create(Employee.prototype);

Object.assign(
  SmartEmployee.prototype,
  {
    salutate: function () {
      var parentSalutate = this._parent.prototype.salutate.call(this);

      return `${parentSalutate} A plat mám ${this._salary} doláčů`;
    },
  },
);

var me = new SmartEmployee(52, 'Vilem', 1000);
console.log(me.getSalary());
console.log(me.salutate());

var Form1 = function (value) {
  this._dom = {
    button: document.createElement('button'),
    input: document.createElement('input'),
  };

  this._dom.input.type = 'text';
  this._dom.input.value = value;

  this._dom.button.textContent = 'klikni';

  document.body.appendChild(this._dom.input);
  document.body.appendChild(this._dom.button);

  var that = this;

  this._dom.button.addEventListener(
    'click',
    function () {
      console.log(that._dom.input.value);
    },
  );
};

// new Form1('kafe');

var Form2 = function (value) {
  this._dom = {
    button: document.createElement('button'),
    input: document.createElement('input'),
  };

  this._dom.input.type = 'text';
  this._dom.input.value = value;

  this._dom.button.textContent = 'klikni';

  document.body.appendChild(this._dom.input);
  document.body.appendChild(this._dom.button);

  this._dom.button.addEventListener(
    'click',
    function () {
      console.log(this._dom.input.value);
    }.bind(this),
  );
};

var Form3 = function (value) {
  this._dom = {
    button: document.createElement('button'),
    input: document.createElement('input'),
  };

  this._dom.input.type = 'text';
  this._dom.input.value = value;

  this._dom.button.textContent = 'klikni';

  document.body.appendChild(this._dom.input);
  document.body.appendChild(this._dom.button);

  this._dom.button.addEventListener(
    'click',
    this._clickHandler.bind(this),
  );
};

Form3.prototype._clickHandler = function () {
  console.log(this._dom.input.value);
};

var Form4 = function (value) {
  this._dom = {
    button: document.createElement('button'),
    input: document.createElement('input'),
  };

  this._dom.input.type = 'text';
  this._dom.input.value = value;

  this._dom.button.textContent = 'klikni';

  document.body.appendChild(this._dom.input);
  document.body.appendChild(this._dom.button);

  this._handlers = {
    onClick: function () {
      console.log(this._dom.input.value);

      this._dom.button.removeEventListener(
        'click',
        this._handlers.onClick,
      );
    }.bind(this),
  };

  this._dom.button.addEventListener(
    'click',
    this._handlers.onClick,
  );
};

var Form5 = function (value) {
  this._dom = {
    button: document.createElement('button'),
    input: document.createElement('input'),
  };

  this._dom.input.type = 'text';
  this._dom.input.value = value;

  this._dom.button.textContent = 'klikni';

  document.body.appendChild(this._dom.input);
  document.body.appendChild(this._dom.button);

  this._dom.button.addEventListener(
    'click',
    this,
  );
  this._dom.input.addEventListener(
    'input',
    this,
  );
};

Form5.prototype.handleEvent = function (event) {
  console.log(event, this);

  switch (event.type) {
    case 'click':
      event.preventDefault();
      console.log(this._dom.input.value);

      this._dom.button.removeEventListener(
        'click',
        this,
      );
      break;
    default:
  }
};

new Form5('kafe 5');
