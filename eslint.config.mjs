import rules from './eslintRules/index.mjs';

const eslintConfig = [
  ...rules,
  {
    rules: {
      'import/extensions': [
        'error',
        'always',
      ],
    },
  },
  {
    files: ['**/*.js', '**/*.mjs'],
    languageOptions: {
      globals: {
        L: 'readonly',
      },
    },
  },
];

export default eslintConfig;
