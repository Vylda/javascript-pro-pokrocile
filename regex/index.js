/* eslint-disable no-unused-vars */

const str = 'Zavolej mi na číslo 123 654 897.';
const ookcode = 'Ook. Ook. Ook. Ook. Ook. Ook. Ook. Ook. Ook. Ook. Ook. Ook. Ook. Ook. Ook. Ook. Ook. Ook. Ook. Ook. Ook! Ook? Ook. Ook? Ook. Ook. Ook. Ook? Ook. Ook. Ook. Ook. Ook. Ook. Ook. Ook? Ook. Ook. Ook. Ook. Ook. Ook. Ook. Ook. Ook. Ook. Ook. Ook. Ook. Ook. Ook. Ook? Ook. Ook. Ook. Ook. Ook. Ook. Ook. Ook. Ook. Ook. Ook. Ook. Ook. Ook. Ook. Ook. Ook. Ook. Ook. Ook. Ook? Ook. Ook? Ook. Ook? Ook. Ook? Ook. Ook! Ook! Ook? Ook! Ook. Ook? Ook. Ook? Ook. Ook. Ook. Ook. Ook. Ook. Ook! Ook.'; // ! v ook jazyce
const text = 'Koupil jsem si kočkodána. S kočkou nemá nic společného a je mi na kočku.';
const presidenti = 'Tomáš Garrigue Masaryk, Edvard Beneš , Emil Hácha ,Edvard Beneš,Klement Gottwald , Antonín Zápotocký;Antonín Novotný ; Ludvík Svoboda; Gustáv Husák ;Václav Havel, Václav Klaus, Miloš Zeman';

const emailRegex = /^([a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*)@((?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?)$/;
const urlRegex = /^(http(s)?:\/\/)?(www\.)?[-a-zA-Z0-9@:%._+~#=]{2,256}\.[a-z]{2,6}\b([-a-zA-Z0-9@:%_+.~#?&//=]*)$/;

const replacement = text.replace(/kočk(..)/g, '$1pes');
console.log(replacement);

const switched = 'Vilém Lipold'.replace(/^(?<sur>.+) (.+)$/, '$<sur>, $1');
console.log(switched);

const presidents = presidenti.split(/ *[,;] */);
console.log(presidents);

console.log('e-mail', emailRegex.test('vilem.lipold@seznam.cz'));
console.log('e-mail', emailRegex.test('vilem.lipold@sešmňam.cz'));

console.log('url', urlRegex.test('http://127.0.0.1:3000/regex/'));
console.log('url', urlRegex.test('http://localhost.lh/regex/'));
